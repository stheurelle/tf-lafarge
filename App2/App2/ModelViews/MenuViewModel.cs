﻿using App2.Common;
using App2.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace App2.ModelViews
{
    public class MenuViewModel
    {
        private INavigation _navigation;
        public MenuViewModel(INavigation navigation)
        {
            _navigation = navigation;
        }

        #region Command => Anomalie
        private ICommand _toAnomalie;
        public ICommand ToAnomalie
        {
            get
            {
                _toAnomalie = _toAnomalie ?? new RelayCommand(GoToAnomalie);
                return _toAnomalie;
            }
        }
        private void GoToAnomalie(object param)
        {
            _navigation.PushAsync(new AnomaliePage());
        }
        #endregion

        #region Command => Alerte
        private ICommand _toAlerte;
        public ICommand ToAlerte
        {
            get
            {
                _toAlerte = _toAlerte ?? new RelayCommand(GoToStatut);
                return _toAlerte;
            }
        }
        private void GoToStatut(object param)
        {
            _navigation.PushAsync(new AlertePage());
        }


        #endregion

        #region Command => Main
        private ICommand _toMain;
        public ICommand ToMain
        {
            get
            {
                _toMain = _toMain ?? new RelayCommand(GoToMain);
                return _toMain;
            }
        }
        private void GoToMain(object param)
        {
            _navigation.PushAsync(new MainPage());
        }


        #endregion

        #region Command => Pause
        private ICommand _toPause;
        public ICommand ToPause
        {
            get
            {
                _toPause = _toPause ?? new RelayCommand(GoToPause);
                return _toPause;
            }
        }
        private void GoToPause(object param)
        {
            _navigation.PushAsync(new EnPausePage());
        }
        #endregion

        #region Command => Messagerie
        private ICommand _toMessagerie;
        public ICommand ToMessagerie
        {
            get
            {
                _toMessagerie = _toMessagerie ?? new RelayCommand(GoToMessagerie);
                return _toMessagerie;
            }
        }
        private void GoToMessagerie(object param)
        {
            App.Current.MainPage.DisplayAlert("ok", "Messagerie", "ok");
        }
        #endregion

        #region Command => Rappel
        private ICommand _toRappel;
        public ICommand ToRappel
        {
            get
            {
                _toRappel = _toRappel ?? new RelayCommand(GoToRappel);
                return _toRappel;
            }
        }
        private void GoToRappel(object param)
        {
            App.Current.MainPage.DisplayAlert("ok", "Rappel", "ok");
        }
        #endregion

        #region Command => Capitaine
        private ICommand _toCapitaine;
        public ICommand ToCapitaine
        {
            get
            {
                _toCapitaine = _toCapitaine ?? new RelayCommand(GoToCapitaine);
                return _toCapitaine;
            }
        }
        private void GoToCapitaine(object param)
        {
            App.Current.MainPage.DisplayAlert("ok", "Capitaine", "ok");
        }
        #endregion
    }
}
