﻿using App2.Common;
using App2.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace App2.ModelViews
{
    public class AlerteMV
    {
        private INavigation _navigation;
        public AlerteMV(INavigation navigation)
        {
            _navigation = navigation;
        }

        #region Command => Main
        private ICommand _toMain;
        public ICommand ToMain
        {
            get
            {
                _toMain = _toMain ?? new RelayCommand(GoToMain);
                return _toMain;
            }
        }
        private void GoToMain(object param)
        {
            _navigation.PushAsync(new MainPage());
        }
        #endregion

        #region Command => Alerte
        private ICommand _toAlerte;
        public ICommand ToAlert
        {
            get
            {
                _toAlerte = _toAlerte ?? new RelayCommand(GoToStatut);
                return _toAlerte;
            }
        }
        private void GoToStatut(object param)
        {
            _navigation.PushAsync(new AlertePage());
        }


        #endregion

        #region Command => Menu
        private ICommand _toMenu;
        public ICommand ToMenu
        {
            get
            {
                _toMenu = _toMenu ?? new RelayCommand(GoToMenu);
                return _toMenu;
            }
        }
        private void GoToMenu(object param)
        {
            _navigation.PushAsync(new MenuPage());
        }


        #endregion
    }
}
