﻿using App2.Common;
using App2.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace App2.ModelViews
{
    public class EnPauseViewModel
    {
        private INavigation _navigation;
        public EnPauseViewModel(INavigation navigation)
        {
            _navigation = navigation;
        }



        #region Command => endPause
        private ICommand _endPause;
        public ICommand EndPauseCommand
        {
            get
            {
                _endPause = _endPause ?? new RelayCommand(DoEndPauseCommand);
                return _endPause;
            }
        }
        private void DoEndPauseCommand(object param)
        {
            _navigation.PushAsync(new MenuPage());
        }
        #endregion
    }
}
