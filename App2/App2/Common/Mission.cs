﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App2.Common
{
    public class Mission
    {
        public int Id { get; set; }
        public string Label { get; set; }
        public string Date { get; set; }

        public string HeureDepart { get; set; }
    }
}
