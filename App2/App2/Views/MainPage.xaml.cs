﻿using App2.Common;
using App2.ModelViews;
using App2.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace App2
{
    public partial class MainPage 
    {
        public MainPage()
        {
            InitializeComponent();
            var model = new MainViewModel(Navigation);
            BindingContext = model;

            lstMissions.ItemTapped += LstMissions_ItemTapped;
            
        }


        //Supression de la sélection de la listview
        private void LstMissions_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            // don't do anything if we just de-selected the row
            if (e.Item == null) return;
            // do something with e.SelectedItem
            ((ListView)sender).SelectedItem = null; // de-select the row
        }

        
    }
}
