﻿using App2.ModelViews;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App2.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EnPausePage : ContentPage
	{
		public EnPausePage ()
		{
			InitializeComponent ();
            var model = new EnPauseViewModel(Navigation);
            BindingContext = model;
		}
	}
}